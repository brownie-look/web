module.exports = {
  devServer: {
    disableHostCheck: true,
    proxy: {
      '^/api': {
        target: 'http://smali.space',
        changeOrigin: true,
        ws: true
      }
    }
  },
  transpileDependencies: ['vuetify']
}
